FROM tiangolo/uwsgi-nginx-flask:python3.6


# Overwrite the `entrypoint.sh`, the original one supported only one port, no certificate and https redirect.
# 修改原entrypoint.sh檔, 原檔僅支援單一port, 且無法https導向, 與發送SSL簽證
COPY ./entrypoint.sh /
