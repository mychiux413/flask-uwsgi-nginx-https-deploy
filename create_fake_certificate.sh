#! /usr/bin/bash
# create certificate `server.key` `server.cert` for local development
openssl req  -nodes -new -x509  -keyout server.key -out server.cert \
    -subj "/C=DE/ST=NRW/L=Berlin/O=My Inc/OU=DevOps/CN=www.example.com/emailAddress=dev@www.example.com"
