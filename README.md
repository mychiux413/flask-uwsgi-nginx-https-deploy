uwsgi-nginx-flask-docker
=========================
##### This project is from:
https://github.com/tiangolo/uwsgi-nginx-flask-docker

Preparation
=======================
* virtualenv
* docker
* docker-compose

QUICK-START
==============
    git clone https://mychiux413@bitbucket.org/mychiux413/flask-uwsgi-nginx-https-deploy.git
    cd flask-uwsgi-nginx-https-deploy
    cd app
    # create virtualenv as pyenv
    bash init_pyenv.sh
    cd ..
    docker-compose up

Test by CURL
==============
    curl --get http://localhost/numpy -L -k # -L: follow redirect, -k: no ssl certificate
    
Test by Browser
==============
* try this url
http://localhost/numpy
