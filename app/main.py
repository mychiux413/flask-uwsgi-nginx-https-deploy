from flask import Flask

import pprint
import os
import sys
app = Flask(__name__)

print('[SYS]: {}'.format(sys.path))

@app.route("/")
def hello():
    return "Hello World!\nTry `/numpy` `/sys` `/static/index.html` also."

import numpy as np
@app.route("/numpy")
def call_numpy():
    arr = np.array([1,2,3])
    reply = 'If I could reply this, the numpy package is import as well, so the virtualenv is working\nARRAY: {}'.format(pprint.pformat(arr))
    return reply

@app.route("/sys")
def env():
    reply = 'Check if the paths of virtualenv is included.\n[SYS]: {}'.format(sys.path)
    return reply


if __name__ == "__main__":
    app.run()
