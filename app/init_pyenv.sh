#! /usr/bin/bash
# don't execute it in docker, but only
# develop your project in virtualenv
virtualenv pyenv --python=python3.6
source pyenv/bin/activate
echo "===== prepare requirements.txt ====="
pip install -r requirements.txt
deactivate

