#! /usr/bin/env bash
set -e
# Get the maximum upload file size for Nginx, default to 0: unlimited
USE_NGINX_MAX_UPLOAD=${NGINX_MAX_UPLOAD:-0}
# Generate Nginx config for maximum upload file size
echo "client_max_body_size $USE_NGINX_MAX_UPLOAD;" > /etc/nginx/conf.d/upload.conf

# Get the number of workers for Nginx, default to 1
USE_NGINX_WORKER_PROCESSES=${NGINX_WORKER_PROCESSES:-1}
# Modify the number of worker processes in Nginx config
sed -i "/worker_processes\s/c\worker_processes ${USE_NGINX_WORKER_PROCESSES};" /etc/nginx/nginx.conf

# Get the URL for static files from the environment variable
USE_STATIC_URL=${STATIC_URL:-'/static'}
# Get the absolute path of the static files from the environment variable
USE_STATIC_PATH=${STATIC_PATH:-'/app/static'}
# Get the listen port for Nginx, default to 80
USE_LISTEN_PORT=${LISTEN_PORT:-80}

# Generate Nginx config first part using the environment variables
echo "server {
    # 80 port一定要導走
    # 80 port must be redirect to $USE_LISTEN_PORT
    listen 80 default_server;
    listen [::]:80 default_server;
    return 302 https://$SERVER_NAME\$uri; # redirect not permanent
    " > /etc/nginx/conf.d/nginx.conf

# If STATIC_INDEX is 1, serve / with /static/index.html directly (or the static URL configured)
if [[ $STATIC_INDEX == 1 ]] ; then
echo "    location = / {
        index $USE_STATIC_URL/index.html;
    }" >> /etc/nginx/conf.d/nginx.conf
fi
# Finish the Nginx config file
echo "}" >> /etc/nginx/conf.d/nginx.conf

echo "server {
  # SSL 設定
  listen ${USE_LISTEN_PORT} ssl default_server;
  listen [::]:${USE_LISTEN_PORT} ssl default_server;

  # 憑證與金鑰的路徑, 不要修改, 而是透過docker-compose.override.yml去修改外部證書路徑
  # the certificate file path, don't modify it here, but through `docker-compose.override.yml` to specify the file path outside of docker.
  ssl_certificate /run/secrets/ssl_certificate;
  ssl_certificate_key /run/secrets/ssl_certificate_key;

  location / {
        try_files \$uri @app;
    }
    location @app {
        include uwsgi_params;
        uwsgi_pass unix:///tmp/uwsgi.sock;
    }
    location $USE_STATIC_URL {
        alias $USE_STATIC_PATH;
  }
}" >> /etc/nginx/conf.d/nginx.conf

exec "$@"
